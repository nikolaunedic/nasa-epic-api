# NASA Epic API images fetcher 

## Setup

Spin up docker container and install composer:

```
docker-compose up -d
docker-compose exec php composer install
```

> NOTE: If you want to run a project with your local PHP (8.0.11 or higher) installation feel free to do so,
> that way you can specify any path, if using docker there's really no sense in using anything but relative paths,
> otherwise images will be saved somewhere in container's filesystem.

Make sure to create `.env` file with `NASA_EPIC_API_URL` and `NASA_EPIC_API_KEY` variables.

## Running the command

```
docker-compose exec php bin/console app:fetch-nasa-photo var/storage 20.05.2021
```

Output:
```
Downloading 5 images started:

Downloading image 1/5
 2919211/2919211 [============================] 100%
Downloading image 2/5
 2799827/2799827 [============================] 100%
Downloading image 3/5
 2952688/2952688 [============================] 100%
Downloading image 4/5
 2700972/2700972 [============================] 100%
Downloading image 5/5
 2662726/2662726 [============================] 100%
Successfully downloaded files to `var/storage/2021-10-09`.

```

### Parameters

| name | description | optional |
| ---- | ----------- | -------- |
| target_dir | Where do you want image to be saved? |  |
| date | Date from which to get an image (dd.mm.yyyy). Last available image will be fetched if this argument is omitted. | ✅ |

## Running tests

```
docker-compose exec php bin/phpunit
```

## Code analysis tooling

### PHPStan

```
docker-compose exec php vendor/bin/phpstan analyze -c phpstan.neon
```

### PHP-CS-fixer

```
docker-compose exec php vendor/bin/php-cs-fixer fix src
```

### Security Checker

```
docker-compose exec php bin/php-security-checker
```
