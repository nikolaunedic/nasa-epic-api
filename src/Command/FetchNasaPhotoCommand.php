<?php

namespace App\Command;

use App\Services\NasaEpicApi\Client as NasaEpicApiClient;
use App\Services\NasaEpicApi\ClientInterface as NasaEpicApiClientInterface;
use App\Services\NasaEpicApi\Exceptions\ConfigurationException;
use App\Services\NasaEpicApi\Exceptions\InvalidApiTokenException;
use DateTime;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

class FetchNasaPhotoCommand extends Command
{
    private const SUGGESTED_DATES_COUNT = 5;
    private const INPUT_DATE_FORMAT = 'd.m.Y';
    private const DIRECTORY_DATE_FORMAT = 'Y-m-d';

    private NasaEpicApiClientInterface $client;
    private Filesystem $filesystem;
    private LoggerInterface $logger;

    public function __construct(NasaEpicApiClientInterface $client, Filesystem $filesystem, LoggerInterface $logger)
    {
        parent::__construct();
        $this->client = $client;
        $this->filesystem = $filesystem;
        $this->logger = $logger;
    }

    protected static $defaultName = 'app:fetch-nasa-photo';

    protected function configure(): void
    {
        $this
            ->setDescription('Fetches the photo from a NASA\'s Epic API for the given date, or latest one available if date is not provided')
            ->addArgument('target_dir', InputArgument::REQUIRED, 'Where do you want image to be saved?')
            ->addArgument('date', InputArgument::OPTIONAL, 'Date from which to get an image (dd.mm.yyyy). Last available image will be fetched if this argument is omitted.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Get input arguments values.
        $date = $input->getArgument('date');
        $targetDir = $input->getArgument('target_dir');

        // It's a simple regex, it caches only simple absolute and relative paths.
        if (!preg_match('/^(\/)?([a-zA-Z0-9]*\/)*([a-zA-Z0-9]*)$/', $targetDir)) {
            $output->writeln('<error>Invalid path. Make sure you provide valid absolute or relative directory path (/home/user/pc/Desktop/images or var/storage).</error>');

            return Command::INVALID;
        }

        if (null !== $date) {
            if (!$date = DateTime::createFromFormat(self::INPUT_DATE_FORMAT, $date)) {
                $output->writeln('<error>Invalid date. Make sure you provide a date in d.m.Y format (e.g. 22.02.2021).</error>');

                return Command::INVALID;
            }
        }

        // Create target directory.
        if (!$this->filesystem->exists($targetDir)) {
            try {
                $this->filesystem->mkdir($targetDir);
            } catch (IOException $e) {
                $output->writeln("<error>Creating directory {$targetDir} failed.</error>");
                $this->logger->error($e);

                return Command::INVALID;
            }
        }

        // Get metadata for a given date(or latest available if date is not provided).
        try {
            $metadata = $this->client->getImageMetadata($date);
        } catch (ConfigurationException $e) {
            $output->writeln('<error>NASA EPIC API must be configured before running a script, check your .env file.</error>');
            $this->logger->error($e);

            return Command::INVALID;
        } catch (InvalidApiTokenException $e) {
            $output->writeln('<error>NASA EPIC API token is not valid.</error>');
            $this->logger->error($e);

            return Command::INVALID;
        } catch (\Exception $e) {
            $output->writeln('<error>Unexpected error occured while communicating with NASA EPIC API, check logs for more info.</error>');
            $this->logger->error($e);

            return Command::INVALID;
        }

        // If no data for given date is returned, display some suggested dates in error message.
        if (empty($metadata)) {
            try {
                $availableDates = $this->client->getAvailableDates();
            } catch (\Exception $e) {
                $output->writeln('<error>Unexpected error occured while communicating with NASA EPIC API, check logs for more info.</error>');
                $this->logger->error($e);

                return Command::INVALID;
            }

            $suggestedDates = implode(", \n", array_slice($availableDates, count($availableDates) - self::SUGGESTED_DATES_COUNT));
            $output->writeln("<error>There are no images for selected date, try one of these dates: \n{$suggestedDates}.</error>");

            return Command::SUCCESS;
        }

        // If date is not provided take date from the image metadata.
        if (null === $date) {
            $date = DateTime::createFromFormat(NasaEpicApiClient::DATETIME_FORMAT, $metadata[0]->date);
        }

        if (false === $date) {
            $output->writeln('<error>Unexpected data format returned from the NASA API. Check their documentation for updates.</error>');

            return Command::FAILURE;
        }

        $fullDirectoryPath = sprintf('%s/%s', $targetDir, $date->format(self::DIRECTORY_DATE_FORMAT));

        // If directory with given date already exists abort.
        if ($this->filesystem->exists($fullDirectoryPath)) {
            $output->writeln('<error>You\'ve already downloaded images for a given date. Try another date.</error>');

            return Command::INVALID;
        }

        try {
            $this->filesystem->mkdir($fullDirectoryPath);
        } catch (IOException $e) {
            $output->writeln("<error>Creating {$fullDirectoryPath} directory failed.</error>");

            return Command::INVALID;
        }

        $photosCount = count($metadata);
        $counter = 1;
        $output->writeln("<info>Downloading {$photosCount} images started:</info>");

        foreach ($metadata as $photoMetadata) {
            $progress = new ProgressBar($output);

            // Create stream context with callback to update ProgressBar.
            $streamContext = stream_context_create([],
                ['notification' => function ($notification_code,
                    $severity,
                    $message,
                    $message_code,
                    $bytes_transferred,
                    $bytes_max) use ($output, $progress, $counter, $photosCount) {
                    switch ($notification_code) {
                    case STREAM_NOTIFY_FILE_SIZE_IS:
                        $output->writeln("\nDownloading image {$counter}/{$photosCount}");
                        $progress->start($bytes_max);
                        break;
                    case STREAM_NOTIFY_PROGRESS:
                        $progress->setProgress($bytes_transferred);
                        break;
                }
                }]);

            $photo = $this->client->getImage($date, $photoMetadata->image, $streamContext);
            $progress->finish();

            if (!$photo) {
                $output->writeln("<error>Downloading image {$counter} failed.</error>");

                return Command::FAILURE;
            }

            $fullFilePath = sprintf('%s/%s.png', $fullDirectoryPath, $photoMetadata->image);

            if (!file_put_contents($fullFilePath, $photo)) {
                $output->writeln("<error>Saving image at {$fullFilePath} failed.</error>");

                return Command::FAILURE;
            }

            ++$counter;
        }

        $output->writeln("\n<info>Successfully downloaded files to `{$fullDirectoryPath}`.</info>");

        return Command::SUCCESS;
    }
}
