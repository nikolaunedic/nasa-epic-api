<?php

namespace App\Services\NasaEpicApi;

use DateTimeInterface;

interface ClientInterface
{
    /**
     * Gets image metadata for a given date or for the latest
     * date with images available if $date param is not passed.
     *
     * @return array<\stdClass>
     */
    public function getImageMetadata(DateTimeInterface $date = null): array;

    /**
     * Gets array of dates for which there are images.
     *
     * @return array<\stdClass>
     */
    public function getAvailableDates(): array;

    /**
     * Downloads the image.
     *
     * @param ?resource $context
     */
    public function getImage(DateTimeInterface $date, string $filename, $context = null): string|false;
}
