<?php

namespace App\Services\NasaEpicApi\Exceptions;

class InvalidApiTokenException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Nasa EPIC API token is not valid.');
    }
}
