<?php

namespace App\Services\NasaEpicApi\Exceptions;

class ConfigurationException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('Nasa EPIC API configuration parameters are missing.');
    }
}
