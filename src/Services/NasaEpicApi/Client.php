<?php

namespace App\Services\NasaEpicApi;

use App\Services\NasaEpicApi\Exceptions\ConfigurationException;
use App\Services\NasaEpicApi\Exceptions\InvalidApiTokenException;
use DateTimeInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Client implements ClientInterface
{
    public const DATE_FORMAT = 'Y-m-d';
    public const DATETIME_FORMAT = 'Y-m-d h:i:s';
    public const API_PREFIX = 'EPIC/api';
    public const ARCHIVE_PREFIX = 'EPIC/archive';

    private HttpClientInterface $client;
    private ?string $apiBaseUrl;
    private ?string $apiKey;
    private bool $clientConfigured;

    public function __construct(HttpClientInterface $client, ?string $apiBaseUrl, ?string $apiKey)
    {
        $this->client = $client;
        $this->apiBaseUrl = $apiBaseUrl;
        $this->apiKey = $apiKey;
        $this->clientConfigured = null !== $apiBaseUrl && null !== $apiKey;
    }

    /**
     * Gets image metadata for a given date or for the latest
     * date with images available if $date param is not passed.
     *
     * @return array<\stdClass>
     */
    public function getImageMetadata(DateTimeInterface $date = null): array
    {
        if (!$this->clientConfigured) {
            throw new ConfigurationException();
        }

        $url = self::API_PREFIX.'/natural';

        if ($date) {
            $url .= '/date/'.$date->format(self::DATE_FORMAT);
        }

        $response = $this->client->request('GET', $url);

        return $this->getResponseContent($response);
    }

    /**
     * Gets array of dates for which there are images.
     *
     * @return array<\stdClass>
     */
    public function getAvailableDates(): array
    {
        if (!$this->clientConfigured) {
            throw new ConfigurationException();
        }

        $response = $this->client->request('GET', self::API_PREFIX.'/natural/available');

        return $this->getResponseContent($response);
    }

    /**
     * Downloads the image.
     *
     * @param ?resource $context
     */
    public function getImage(DateTimeInterface $date, string $filename, $context = null): string|false
    {
        if (!$this->clientConfigured) {
            throw new ConfigurationException();
        }

        $url = sprintf('%s/%s/natural/%s/%s/%s/png/%s.png?api_key=%s',
            $this->apiBaseUrl,
            self::ARCHIVE_PREFIX,
            $date->format('Y'),
            $date->format('m'),
            $date->format('d'),
            $filename,
            $this->apiKey
        );

        return file_get_contents($url, false, $context);
    }

    /**
     * Gets the response content and handles exceptions.
     *
     * @return array<\stdClass>
     */
    private function getResponseContent(ResponseInterface $response): array
    {
        try {
            $data = json_decode($response->getContent());

            if (!is_array($data)) {
                throw new \Exception('Failed to decode data returned from the NASA EPIC API.');
            }

            return $data;
        } catch (ClientExceptionInterface $e) {
            switch ($response->getStatusCode()) {
                case 404:
                    throw new \Exception('Requested route not found on NASA EPIC API.');
                case 403:
                    throw new InvalidApiTokenException();
                default:
                    throw new \Exception('Unexpected error happened while trying to communicate with NASA EPI API.');
            }
        } catch (TransportExceptionInterface $e) {
            throw new \Exception('Unexpected error happened while trying to communicate with NASA EPI API.');
        }
    }
}
