<?php

namespace App\Tests\Unit;

use App\Services\NasaEpicApi\Client as NasaEpicApiClient;
use App\Services\NasaEpicApi\Exceptions\ConfigurationException;
use App\Services\NasaEpicApi\Exceptions\InvalidApiTokenException;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class NasaEpicApiClientTest extends TestCase
{
    protected HttpClientInterface $baseClient;

    protected function setUp(): void
    {
        parent::setUp();
        $this->baseClient = $this->createMock(HttpClientInterface::class);
    }

    public function testNotConfiguredFails()
    {
        $nasaEpicApiClient = new NasaEpicApiClient($this->baseClient, null, null);

        $this->expectException(ConfigurationException::class);
        $nasaEpicApiClient->getImageMetadata();
    }

    public function testImageMetadataFetching()
    {
        $response = $this->createMock(ResponseInterface::class);

        $nasaEpicApiClient = new NasaEpicApiClient($this->baseClient, 'some_ulr', 'some_api_key');
        $this->baseClient
            ->expects($this->once())
            ->method('request')
            ->with('GET', NasaEpicApiClient::API_PREFIX.'/natural')
            ->willReturn($response);

        $response
            ->expects($this->once())
            ->method('getContent')
            ->willReturn('[]');

        $data = $nasaEpicApiClient->getImageMetadata();
        $this->assertEquals($data, []);
    }

    public function testAvailableDatesFetching()
    {
        $response = $this->createMock(ResponseInterface::class);

        $nasaEpicApiClient = new NasaEpicApiClient($this->baseClient, 'some_ulr', 'some_api_key');
        $this->baseClient
            ->expects($this->once())
            ->method('request')
            ->with('GET', NasaEpicApiClient::API_PREFIX.'/natural/available')
            ->willReturn($response);

        $response
            ->expects($this->once())
            ->method('getContent')
            ->willReturn('[]');

        $data = $nasaEpicApiClient->getAvailableDates();
        $this->assertEquals($data, []);
    }

    public function testExceptionsHandling()
    {
        $clientException = $this->createMock(ClientExceptionInterface::class);
        $transportException = $this->createMock(TransportExceptionInterface::class);
        $genericException = $this->createMock(\Throwable::class);

        $cases = [
            [
                'exception' => $clientException,
                'responseStatusCode' => 403,
                'expectedExceptionThrown' => InvalidApiTokenException::class,
                'expectedExceptionMessage' => 'Nasa EPIC API token is not valid.',
            ],
            [
                'exception' => $clientException,
                'responseStatusCode' => 404,
                'expectedExceptionThrown' => \Exception::class,
                'expectedExceptionMessage' => 'Requested route not found on NASA EPIC API.',
            ],
            [
                'exception' => $clientException,
                'responseStatusCode' => 500,
                'expectedExceptionThrown' => \Exception::class,
                'expectedExceptionMessage' => 'Requested route not found on NASA EPIC API.',
            ],
            [
                'exception' => $transportException,
                'responseStatusCode' => null,
                'expectedExceptionThrown' => \Exception::class,
                'expectedExceptionMessage' => 'Unexpected error happened while trying to communicate with NASA EPI API',
            ],
            [
                'exception' => $genericException,
                'responseStatusCode' => null,
                'expectedExceptionThrown' => \Exception::class,
                'expectedExceptionMessage' => 'Unexpected error happened while trying to communicate with NASA EPI API',
            ],
        ];

        foreach ($cases as $case) {
            $this->testSingleExceptionCase($case);
        }
    }

    private function testSingleExceptionCase(array $case)
    {
        $response = $this->createMock(ResponseInterface::class);

        $nasaEpicApiClient = new NasaEpicApiClient($this->baseClient, 'some_ulr', 'some_api_key');
        $this->baseClient
            ->expects($this->once())
            ->method('request')
            ->with('GET', NasaEpicApiClient::API_PREFIX.'/natural')
            ->willReturn($response);

        $response
            ->expects($this->once())
            ->method('getContent')
            ->will($this->throwException($case['exception']));

        if (null !== $case['responseStatusCode']) {
            $response
                ->expects($this->once())
                ->method('getStatusCode')
                ->willReturn($case['responseStatusCode']);
        }

        $this->expectException($case['expectedExceptionThrown']);
        $this->expectExceptionMessage($case['expectedExceptionMessage']);

        $nasaEpicApiClient->getImageMetadata();
    }
}
