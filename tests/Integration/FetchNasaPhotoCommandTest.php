<?php

/**
 * Mocking built-in function.
 *
 * @see https://stackoverflow.com/questions/39706887/phpunit-mock-an-inbuilt-function#answer-56386772
 */

namespace App\Command {
    function file_put_contents(string $filename, mixed $data, int $flags = 0, $context = null): int
    {
        return 1;
    }
}

namespace App\Tests\Integration {
    use App\Command\FetchNasaPhotoCommand;
    use App\Services\NasaEpicApi\ClientInterface as NasaEpicApiClientInterface;
    use Psr\Log\LoggerInterface;
    use Symfony\Bundle\FrameworkBundle\Console\Application;
    use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Tester\CommandTester;
    use Symfony\Component\Filesystem\Filesystem;

    class FetchNasaPhotoCommandTest extends KernelTestCase
    {
        private const FETCH_NASA_PHOTO_COMMAND = 'app:fetch-nasa-photo';

        private Filesystem $filesystem;
        private NasaEpicApiClientInterface $client;
        private LoggerInterface $logger;
        private CommandTester $commandTester;

        public function setUp(): void
        {
            $this->client = $this->createMock(NasaEpicApiClientInterface::class);
            $this->filesystem = $this->createMock(Filesystem::class);
            $this->logger = $this->createMock(LoggerInterface::class);

            $imageMetadata = new \stdClass();
            $imageMetadata->image = 'test.png';
            $imageMetadata->date = '2021-12-30 12:22:11';

            $this->client
                ->method('getImageMetadata')
                ->willReturn([$imageMetadata]);

            $this->client
                ->method('getImage')
                ->willReturn('image_data');

            $kernel = self::bootKernel();
            $application = new Application($kernel);
            $application->add(new FetchNasaPhotoCommand($this->client, $this->filesystem, $this->logger));

            $command = $application->find(self::FETCH_NASA_PHOTO_COMMAND);
            $this->commandTester = new CommandTester($command);
        }

        public function testSuccess()
        {
            $commnandRetrunValue = $this->commandTester->execute([
                'target_dir' => 'var/storage',
            ]);

            $output = $this->commandTester->getDisplay();
            $this->assertEquals($commnandRetrunValue, Command::SUCCESS);
            $this->assertStringContainsString('Successfully downloaded files to `var/storage', $output);
        }

        public function testPathValidation()
        {
            $paths = [
                [
                    'path' => '?..',
                    'expectedReturnValue' => Command::INVALID,
                    'expectedMessage' => 'Invalid path.',
                ],
                [
                    'path' => '../../',
                    'expectedReturnValue' => Command::INVALID,
                    'expectedMessage' => 'Invalid path.',
                ],
                [
                    'path' => 'var/storage',
                    'expectedReturnValue' => Command::SUCCESS,
                    'expectedMessage' => 'Successfully downloaded files to ',
                ],
                [
                    'path' => '/var/www/app/var/storage/nasa',
                    'expectedReturnValue' => Command::SUCCESS,
                    'expectedMessage' => 'Successfully downloaded files to ',
                ],
            ];

            foreach ($paths as $path) {
                $this->testPath($path['path'], $path['expectedReturnValue'], $path['expectedMessage']);
            }
        }

        private function testPath($path, $expectedReturnValue, $expectedMessage)
        {
            $commnandRetrunValue = $this->commandTester->execute([
                'target_dir' => $path,
            ]);

            $output = $this->commandTester->getDisplay();
            $this->assertEquals($commnandRetrunValue, $expectedReturnValue);
            $this->assertStringContainsString($expectedMessage, $output);
        }

        public function testDateValidation()
        {
            $dates = [
                [
                    'date' => '20-01-2021',
                    'expectedReturnValue' => Command::INVALID,
                    'expectedMessage' => 'Invalid date.',
                ],
                [
                    'date' => '20/02/2010',
                    'expectedReturnValue' => Command::INVALID,
                    'expectedMessage' => 'Invalid date.',
                ],
                [
                    'date' => '20.01.2021',
                    'expectedReturnValue' => Command::SUCCESS,
                    'expectedMessage' => 'Successfully downloaded files to ',
                ],
            ];

            foreach ($dates as $date) {
                $this->testDate($date['date'], $date['expectedReturnValue'], $date['expectedMessage']);
            }
        }

        private function testDate($date, $expectedReturnValue, $expectedMessage)
        {
            $commnandRetrunValue = $this->commandTester->execute([
                'target_dir' => 'var/storage',
                'date' => $date,
            ]);

            $output = $this->commandTester->getDisplay();
            $this->assertEquals($commnandRetrunValue, $expectedReturnValue);
            $this->assertStringContainsString($expectedMessage, $output);
        }

        /**
         * Makes sure that command fails when second call to `exists` function,
         * which check if final path already exists, fails.
         */
        public function testFailsOnConsecutiveRuns()
        {
            $filesystem = $this->createMock(Filesystem::class);
            $filesystem->expects($this->exactly(2))
                ->method('exists')
                ->willReturnOnConsecutiveCalls(false, true);

            $kernel = self::bootKernel();
            $application = new Application($kernel);
            $application->add(new FetchNasaPhotoCommand($this->client, $filesystem, $this->logger));

            $command = $application->find(self::FETCH_NASA_PHOTO_COMMAND);
            $this->commandTester = new CommandTester($command);

            $commnandRetrunValue = $this->commandTester->execute([
                'target_dir' => 'var/storage',
                'date' => '20.01.2021',
            ]);

            $output = $this->commandTester->getDisplay();
            $this->assertEquals($commnandRetrunValue, Command::INVALID);
            $this->assertStringContainsString('You\'ve already downloaded images for a given date. Try another date.', $output);
        }

        public function testShowSuggestionsForEmptyDates()
        {
            $client = $this->createMock(NasaEpicApiClientInterface::class);

            $client
                ->expects($this->once())
                ->method('getImageMetadata')
                ->willReturn([]);

            $client
                ->expects($this->once())
                ->method('getAvailableDates')
                ->willReturn(['20.12.2021']);

            $kernel = self::bootKernel();
            $application = new Application($kernel);
            $application->add(new FetchNasaPhotoCommand($client, $this->filesystem, $this->logger));

            $command = $application->find(self::FETCH_NASA_PHOTO_COMMAND);
            $this->commandTester = new CommandTester($command);

            $commnandRetrunValue = $this->commandTester->execute([
                'target_dir' => 'var/storage',
                'date' => '20.01.2021',
            ]);

            $output = $this->commandTester->getDisplay();
            $this->assertEquals($commnandRetrunValue, Command::SUCCESS);
            $this->assertStringContainsString('There are no images for selected date, try one of these dates:', $output);
            $this->assertStringContainsString('20.12.2021', $output);
        }
    }
}
